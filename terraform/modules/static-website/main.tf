# Terraform configuration
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.69.0"
    }
  }
}
data "aws_vpc" "default" {
  default = true
}

#public bucket with content
resource "aws_s3_bucket" "static_website" {
  bucket = var.bucket_name
  acl    = "public-read"
  tags   = var.tags
  website {
    index_document = "index.html"
  }
}

resource "aws_s3_bucket_object" "html_file" {
  bucket = aws_s3_bucket.static_website.id
  key    = "index.html"
  acl    = "public-read"
  source = "modules/static-website/files/index.html"
  etag = filemd5("modules/static-website/files/index.html")
  content_type = "text/html"
}

resource "aws_s3_bucket_object" "css_file" {
  bucket = aws_s3_bucket.static_website.id
  key    = "style.css"
  acl    = "public-read"
  source = "modules/static-website/files/style.css"
  etag = filemd5("modules/static-website/files/style.css")
  content_type = "text/css"
}

resource "aws_s3_bucket_object" "img_files" {
  for_each = toset( ["bob", "chose", "img01", "malle", "large", "rouge"])
  bucket = aws_s3_bucket.static_website.id
  key    = "${each.key}.jpeg"
  acl    = "public-read"
  source = "modules/static-website/files/${each.key}.jpeg"
  etag = filemd5("modules/static-website/files/${each.key}.jpeg")
  content_type = "img/jpeg"
}

# ec2 with content
data "aws_ami" "amazon_linux" {
  owners           = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.20210326.0-x86_64-ebs"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_spot_instance_request" "static_website" {
  ami                    = data.aws_ami.amazon_linux.image_id
  spot_price             = var.spot_price
  instance_type          = var.instance_type
  key_name               = aws_key_pair.static_website.key_name
  user_data              = file("modules/static-website/files/userdata.sh")
  vpc_security_group_ids = [aws_security_group.static_website.id]

  tags = {
    Name = "test Cloudfront"
  }
}



resource "aws_key_pair" "static_website" {
  key_name   = "static_website_poc_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCw2inbQJ91yVNGgT8YgliCN2aixV0/tUPBhuWqyZQCa1JjdbqBxHB9fDgY14fr+wFPfO8jWFAxYbBvW1D7Je52Nf0Z9S/y3/BW3ojm0aAKqvUfg8y2dSVt/LX+NPZMdaaCWDj1F3vRWr0vItwuu9pFCwkBuyfiqfnPyhxryNN93Xz+KqXhW7V7TpOUfDR6D7GzToGJhlGcRCkWsTO3D0FLxTru2OqQ9n0L9sHpuhXCmSCcIWYvuovBMH7eQqAWTw8Ls1P5uDM3uZm0J8JsPNZYRv4ydfeEBXT4YP9jmsqL9k26TK7wtFFRxSpM6VgYPMDhaZvX11JGotPWXMFH5p57 alexandregirault@MacBook-Pro-de-Babylone.local"
}

resource "aws_security_group" "static_website" {
  name        = "allow_https"
  description = "Allow https from internet"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "allow http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "allow https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ssh from internet"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "test Cloudfront"
  }
}



# Cloudfront 

resource "aws_cloudfront_distribution" "static_website_s3" {
  count = var.cloudfront_enabled ? 1 : 0
  origin {
    domain_name = aws_s3_bucket.static_website.bucket_regional_domain_name
    origin_id   = "static_website_s3"
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "static_website"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "static_website"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    function_association {
      event_type   = "viewer-request"
      function_arn = aws_cloudfront_function.rewrite-index-html[0].arn
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Environment = "production"
  }

  viewer_certificate {
    cloudfront_default_certificate = false
    acm_certificate_arn            = var.acm_certificate_arn
    minimum_protocol_version       = "TLSv1"
    ssl_support_method             = "sni-only"
  }
}

resource "aws_cloudfront_function" "rewrite-index-html" {
  count   = var.cloudfront_enabled ? 1 : 0
  name    = "rewrite-index-html"
  runtime = "cloudfront-js-1.0"
  comment = "rewrite '/' to '/index.html'"
  publish = true
  code    = file("./modules/static-website/rewrite-index-html.js")
}