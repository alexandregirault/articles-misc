# Output variable definitions

output "arn" {
  description = "ARN of the bucket"
  value       = aws_s3_bucket.static_website.arn
}

output "name" {
  description = "Name (id) of the bucket"
  value       = aws_s3_bucket.static_website.id
}

output "domain" {
  description = "Domain name of the bucket"
  value       = aws_s3_bucket.static_website.website_domain
}
