#!/bin/bash
amazon-linux-extras enable nginx1
yum clean metadata
yum -y install nginx

rm -Rf /usr/share/nginx/html/*
cd /usr/share/nginx/html/
wget -E -H -k -K -p https://static-website-24stechblog-article-us.s3.us-west-1.amazonaws.com/index.html#photos -P /tmp/
cp /tmp/static-website-24stechblog-article*/* /usr/share/nginx/html/

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/certs/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -subj "/C=FR/ST=IleDeFrance/L=Paris/O=poccdnper/CN=www.poccdnper.io"

echo "user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;
include /usr/share/nginx/modules/*.conf;
events {
    worker_connections 1024;
}
http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    access_log  /var/log/nginx/access.log  main;
    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 4096;
    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;
    include /etc/nginx/conf.d/*.conf;
    server {
    	listen       80 ;
        listen       [::]:80 ;
        server_name  _;
        root         /usr/share/nginx/html;
    }
    server {
        listen       443 ssl http2;
        listen       [::]:443 ssl http2;
        server_name  _;
        root         /usr/share/nginx/html;
        ssl_certificate \"/etc/ssl/certs/nginx-selfsigned.crt\";
        ssl_certificate_key \"/etc/ssl/certs/nginx-selfsigned.key\";
        include /etc/nginx/default.d/*.conf;
        error_page 404 /404.html;
            location = /40x.html {
        }
        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
}
" > /etc/nginx/nginx.conf

systemctl start nginx

