# Input variable definitions

variable "bucket_name" {
  description = "Name of the s3 bucket. Must be unique."
  type        = string
}
variable "acm_certificate_arn" {
  type = string
}

variable "tags" {
  description = "Tags to set on the bucket."
  type        = map(string)
  default = {
    Name        = "simple static site bucket"
    Environment = "test"
  }
}

variable "cloudfront_enabled" {
  type    = bool
  default = false
}

variable "instance_type" {
  description = "aws instance type"
  default     = "c5.large"
}

variable "spot_price" {
  description = "max price/hour"
  default     = "0.05"
}

variable "instance_AMI" {
  description = "Amazon Linux 2 AMI 2.0.20210326.0 x86_64 HVM ebs"
  default     = "ami-07d53a1fb3f37ae28"
}

