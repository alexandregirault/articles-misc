variable "bucket_name_prefix" {
  description = "Name of the s3 bucket. Must be unique."
  type        = string
  default     = "static-website-24stechblog-article"
}