provider "aws" {
  region = "eu-west-3"
}

provider "aws" {
  alias  = "acm_region"
  region = "us-east-1"
}

provider "aws" {
  alias  = "us"
  region = "us-west-1"
}

provider "aws" {
  alias  = "ir"
  region = "eu-west-1"
}

provider "aws" {
  alias  = "africa"
  region = "af-south-1"
}

provider "aws" {
  alias  = "asiap"
  region = "ap-southeast-2"
}

provider "aws" {
  alias  = "asiahk"
  region = "ap-east-1"
}

terraform {
  backend "s3" {
    bucket         = "tf-states-labawsalex"
    key            = "global/s3/aws-poc.state"
    region         = "eu-west-3"
    dynamodb_table = "terraforms_locks"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.69.0"
    }
  }
  required_version = "1.1.0"
}