data "aws_acm_certificate" "personnal_certificate" {
  domain   = "*.alexcloudlab.org"
  statuses = ["ISSUED"]
  provider = aws.acm_region
}

data "aws_s3_bucket" "website_content" {
  bucket = "hugo-sandbox"
}

module "static_website_us" {
  source              = "./modules/static-website"
  bucket_name         = "${var.bucket_name_prefix}-us"
  acm_certificate_arn = data.aws_acm_certificate.personnal_certificate.arn
  providers = {
    aws = aws.us
  }
}

module "static_website_irlande" {
  source = "./modules/static-website"
  bucket_name = "${var.bucket_name_prefix}-irlande"
  acm_certificate_arn = data.aws_acm_certificate.personnal_certificate.arn
  providers = {
    aws = aws.ir
  }
}

#module "static_website_asiahk" {
#  source = "./modules/static-website"
#  bucket_name = "${var.bucket_name_prefix}-asiahk"
#  acm_certificate_arn = data.aws_acm_certificate.already_issued_certificate.arn
#  providers = {
#    aws = aws.asiahk
#  }
#}